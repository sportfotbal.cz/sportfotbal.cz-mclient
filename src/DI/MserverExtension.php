<?php


namespace Andering\MClient\DI;

use Nette;

class MClientExtension extends Nette\DI\CompilerExtension {

    private $defaults = [
        'exportsDir' => '%wwwDir%',
        'exports'    => []
    ];

    public function loadConfiguration()
    {
        parent::loadConfiguration();

        $builder = $this->getContainerBuilder();
        $config = $this->getConfig($this->defaults);

        $builder->addDefinition($this->prefix('storage'))
            ->setClass('\Andering\MClient\Storage', [$config['exportsDir']]);

        foreach ($config['exports'] as $export => $class) {
            if (!class_exists($class)) {
            }
            $builder->addDefinition($this->prefix($export))
                ->setClass($class);

        }

        if (class_exists('\Symfony\Component\Console\Command\Command')) {
            $builder->addDefinition($this->prefix('command'))
                ->setClass('Andering\MClient\Command\MClientCommand', [$config])
                ->addTag('kdyby.console.command');
        }
    }
}
