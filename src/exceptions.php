<?php

namespace Andering\MClient;

class LogicException extends \LogicException
{
	private $return;

	public function setBody($return)
	{
		$this->return = $return;
	}

	public function getBody()
	{
		return $this->return;
	}
}


class BadRequestException extends LogicException
{

}

class UnauthorizedException extends LogicException
{

}

class ForbiddenException extends LogicException
{

}

class NotFoundException extends LogicException
{

}

class MethodNotAllowedException extends LogicException
{

}

class RequestTimeoutException extends LogicException
{

}

class InternalServerErrorException extends LogicException
{

}

class BadGatewayException extends LogicException
{

}

class ServiceUnavailableException extends LogicException
{

}

class GatewayTimeoutException extends LogicException
{

}

class HTTPVersionNotSupportedException extends LogicException
{

}

class ExceptionNotRecognizedException extends LogicException
{

}
