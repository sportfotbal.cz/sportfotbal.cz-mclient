<?php

namespace Andering\MClient\Definition;

class Carrier
{
	const CP = 'cp';
	const DPD = 'dpd';
	const GEIS = 'geis';
	const GLS = 'gls';
	const INTIME = 'intime';
	const PBH = 'pbh';
	const PPL = 'ppl';
	const TOPTRANS = 'toptrans';
	const ULOZENKA = 'ulozenka';
	const ZASILKOVNA = 'zasilkovna';
}
