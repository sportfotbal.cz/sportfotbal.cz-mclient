<?php

namespace Andering\MClient\Command;

use Symfony\Component\Console\Command\Command,
	Symfony\Component\Console\Input\InputInterface,
	Symfony\Component\Console\Output\OutputInterface,
	Symfony\Component\Console\Input\InputOption;

use Nette,
	Andering;

class MClientCommand extends Command {

	/** @var \Nette\DI\Container */
	private $container;

	/** @var array */
	private $config;

	public function __construct(array $config = [], Nette\DI\Container $container)
	{
		parent::__construct();

		$this->container = $container;
		$this->config = $config;
	}

	protected function configure()
	{
		$this->setName('MClient:export')
			->setDescription('Export product mclient')
			->addOption('show', 's', InputOption::VALUE_NONE, 'Print available exports')
			->addOption('mclient', 'f', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$show = $input->getOption('show');
		$mclients = $input->getOption('mclient');

		if ($show) {
			$output->writeln('Available exports:');

			foreach ($this->config['exports'] as $k => $v) {
				if ($v) {
					$output->writeln('- ' . $k);
				}
			}
		}

		$mclients = $mclients ?: array_keys($this->config['exports']);
		if (count($mclients)) {
			foreach ($mclients as $mclient) {
				if (!isset($this->config['exports'][$mclient]) || !$this->config['exports'][$mclient]) {
					$output->writeln('Generator for ' . $mclient . ' doesn\'t exist');
				}

				$generator = $this->container->getService('mclient.' . $mclient);

				$generator->save($mclient . '.xml');
				$output->writeln('MClient ' . $mclient . ' done');
			}
		}
	}
}
