<?php

namespace Andering\MClient;

use GuzzleHttp,SimpleXMLElement,Latte;
use Andering\MClient\Definition\Carrier;
use Tracy\Debugger;
use Andering\MClient\map;
use DateTime,DateTimeZone,DateInterval;


Debugger::$showBar = TRUE;

class MClient
{
	private $config;
	private $client;
	private $latte;


	public function __construct($config)
	{

		$this->config = [
			'url' => 'http://77.93.197.86:7276/xml',
			'method' => 'POST',
			'auth' => []
		];

		$this->config = $config + $this->config;

		$latte = new Latte\Engine;
		$latte->setTempDirectory(sys_get_temp_dir());

		$this->latte = $latte;
	}

	public function getClient()
	{
		$this->client = new GuzzleHttp\Client($this->config);
		return $this->client;
	}

	private function call($data = [])
	{
		try {
			$client = $this->getClient();

			$r = $client->request($this->config['method'],$this->config['url'],
				[
					'headers' => [
						'STW-Authorization' => "Basic ".base64_encode($this->config['auth'][0].":".$this->config['auth'][1]),
						'Content-Type' => 'text/xml',
						'Accept-Encoding' => 'gzip, deflate'
					],
					'body'=>$data['body'],
					'debug'=>false
				]
			);

		} catch (\GuzzleHttp\Exception\ClientException $e) {
			switch($e->getResponse()->getStatusCode())
			{
				case 400:
					$e = new BadRequestException("Požadavek nemůže být vyřízen, poněvadž byl syntakticky nesprávně zapsán.");
					$e->setBody($body);
					throw $e;
					break;
				case 401:
					$e = new UnauthorizedException("Používán tam, kde je vyžadována autentifikace, ale nebyla zatím provedena.");
					$e->setBody($body);
					throw $e;
					break;
				case 403:
					$e = new ForbiddenException("Požadavek byl legální, ale server odmítl odpovědět.");
					$e->setBody($body);
					throw $e;
					break;
				case 404:
					$e = new NotFoundException("Požadovaný dokument nebyl nalezen.");
					$e->setBody($body);
					throw $e;
					break;
				case 405:
					$e = new MethodNotAllowedException("Požadavek byl zavolán na zdroj s metodou, kterou nepodporuje.");
					$e->setBody($body);
					throw $e;
					break;
				case 408:
					$e = new RequestTimeoutException("Vypršel čas vyhrazený na zpracování požadavku.");
					$e->setBody($body);
					throw $e;
					break;
				case 500:
					$e = new InternalServerErrorException("Při zpracovávání požadavku došlo k blíže nespecifikované chybě.");
					$e->setBody($body);
					throw $e;
					break;
				case 502:
					$e = new BadGatewayException("Proxy server nebo brána obdržely od serveru neplatnou odpověď.");
					$e->setBody($body);
					throw $e;
					break;
				case 503:
					$e = new ServiceUnavailableException("Služba je dočasně nedostupná.");
					$e->setBody($body);
					throw $e;
					break;
				case 504:
					$e = new GatewayTimeoutException("Proxy server nedostal od cílového serveru odpověď v daném čase.");
					$e->setBody($body);
					throw $e;
					break;
				case 505:
					$e = new HTTPVersionNotSupportedException("Server nepodporuje verzi protokolu HTTP použitou v požadavku.");
					$e->setBody($body);
					throw $e;
					break;
				default:
					$e = new ExceptionNotRecognizedException("Nepodařilo se rozeznat chybový stav.");
					$e->setBody($body);
					throw $e;
					break;
			}
		}
		return new SimpleXMLElement($r->getBody());
	}

	private function tr_recursive($xml,$map)
	{

		list($propertyName,$xmlXpath,$closure,$childrenCollection) = $map;

		$out1 = [];
		foreach($xml->xpath($xmlXpath) as $x){
			$out2 = [];
			if(!count($childrenCollection)) {
				$out2 = $x;
			} else {
				foreach($childrenCollection as $children) {
					$out2 = $out2 + $this->tr_recursive($x,$children);
				}
			}
			$out2 = $closure($out2);
			$out1[] = $out2;
		}


		if(is_array(reset($childrenCollection))){
			$out1 = (array) $out1;
		} else {

			if(count($out1) > 1) {
				$out1 = $out1;
			} else if(count($out1) == 1) {
				$out1 = current($out1);
			} else {
				$out1 = null;
			}
		}

		return [$propertyName => $out1];
	}


	public function buildRequest($template,$data)
	{
		$request = $this->latte->renderToString(__DIR__."/latte/{$template}.latte", $data);
		return $request;
	}

	public function buildResponse($template,$data)
	{
		$template = "\\Andering\\MClient\\map\\{$template}";
		$response = new $template();

		return $response->map;
	}

	public function get($data,$request,$response)
	{

		$object = $this->tr_recursive(
			$this->call(['body' => $this->buildRequest($request,$data)]),
			$this->buildresponse($response,$data)
		);



		return $object;
	}

	public function setInvoice($data = [],$response = 'flatInvoiceRespond',$request = 'import.invoice')
	{
		$data = $data + ['invoiceType' => 'issuedInvoice'];
		return $this->get($data,$request,$response);
	}

	public function getInvoice($data = [],$response = 'listInvoiceRequest',$request = 'export.invoice')
	{
		$data = $data + ['invoiceType' => 'issuedInvoice'];
		return $this->get($data,$request,$response);
	}

	public function getReturn($data = [],$response = 'listInvoiceRequest',$request = 'export.invoice')
	{
		$data = $data + ['invoiceType' => 'issuedCorrectiveTax'];
		return $this->get($data,$request,$response);
	}

	public function getProdejka($data = [],$response = 'listProdejkaRequest',$request = 'export.prodejka')
	{
		return $this->get($data,$request,$response);
	}


	public function setStock($data = [],$response = 'flatStockRespond',$request = 'import.stock')
	{
		return $this->get($data,$request,$response);
	}

	public function getStock($data = [],$response = 'listStockRequest',$request = 'export.stock')
	{
		return $this->get($data,$request,$response);
	}


	public function getTest($data = [],$response = 'listStockRequest',$request = 'export.test')
	{
		return $this->get($data,$request,$response);
	}

}
