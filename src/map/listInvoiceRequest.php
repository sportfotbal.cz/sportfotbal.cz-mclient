<?php

namespace Andering\MClient\map;

use DateTime;

class listInvoiceRequest
{

	public $map = null;

	public function __construct()
	{

		$this->map =
			['invoice','//lst:invoice',function($value){  return (object) $value; },
				[
					['invoiceHeader','./inv:invoiceHeader',function($value){ return (object) $value;},
						[
							['id','./inv:id',function($value){ return (string) $value; },[]],
							['date','./inv:date',function($value){ return new DateTime($value); },[]]
						],
					],
					['invoiceDetail','./inv:invoiceDetail',function($value){  return (object) $value; },
						[
							['invoiceItem','./inv:invoiceItem',function($value){ return (object) $value; },
								[
									['contract','./inv:contract',function($value){ return (object) $value; },
										[
											['ids','./typ:ids',function($value){ return (string) $value; },[]]
										]
									],
									['quantity','./inv:quantity',function($value){ return (string) $value; },[]],
									['discountPercentage','./inv:discountPercentage',function($value){ return (string) $value; },[]],
									['stockItem','./inv:stockItem',function($value){ return (object) $value; },
										[
											['stockItem','./typ:stockItem',function($value){ return (object) $value; },
												[
													['ean','./typ:EAN',function($value){ return (string) $value; },[]],
												]
											]
										]
									],
									['homeCurrency','./inv:homeCurrency',function($value){ return (object) $value; },
										[
											['price','./typ:price',function($value){ return (string) $value; },[]]

										]
									]
								]
							],
						]
					],
					['invoiceSummary','./inv:invoiceSummary',function($value){  return (object) $value;},
						[
							['homeCurrency','inv:homeCurrency',function($value){return (object) $value; },[]]
						]
					]
				]
			];

	}

}

/*
		$map =
			['invoice','//lst:invoice',function($value){  return (object) $value; },
				[

					['id','./inv:invoiceHeader/inv:id',function($value){ return (string) $value; },[]],
					['date','./inv:invoiceHeader/inv:date',function($value){ return new DateTime($value); },[]],
					['invoiceItem','./inv:invoiceDetail/inv:invoiceItem',function($value){ return (object) $value; },
						[
							['contract','./inv:contract/typ:ids',function($value){ return (string) $value; },[]],
							['quantity','./inv:quantity',function($value){ return (string) $value; },[]],
							['discountPercentage','./inv:discountPercentage',function($value){ return (string) $value; },[]],
							['ean','./inv:stockItem/typ:stockItem/typ:EAN',function($value){ return (string) $value; },[]],
							['price','./inv:homeCurrency/typ:price',function($value){ return (string) $value; },[]]
						]
					]
				]
			];
*/
