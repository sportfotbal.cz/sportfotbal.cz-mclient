<?php

namespace Andering\MClient\map;

use DateTime;

class flatStockRespond
{

	public $map = null;

	public function __construct()
	{

		$this->map =
			['stock','//lStk:stock',function($value){  return (object) $value; },
				[
					['weightedPurchasePrice','./stk:stockHeader/stk:weightedPurchasePrice',function($value){ return (string) $value; },[]],
					['name','./stk:stockHeader/stk:name',function($value){ return (string) $value; },[]],
					['count','./stk:stockHeader/stk:count',function($value){ return (string) $value; },[]],
					['ean','./stk:stockHeader/stk:EAN',function($value){ return (string) $value; },[]],
					['id','./stk:stockHeader/stk:id',function($value){ return (string) $value; },[]],
					['code','./stk:stockHeader/stk:code',function($value){ return (string) $value; },[]],
					['note','./stk:stockHeader/stk:nameComplement',function($value){ return (string) $value; },[]],
					['supplier','./stk:stockHeader/stk:supplier/typ:id',function($value){ return (string) $value; },[]]
				],

			];

	}

}
