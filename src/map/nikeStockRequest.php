<?php

namespace Andering\MClient\map;

use DateTime;

class nikeStockRequest
{

	public $map = null;

	public function __construct()
	{

		$this->map =
			['stock','//lStk:stock',function($value){  return (object) $value; },
				[
					['weightedPurchasePrice','./stk:stockHeader/stk:weightedPurchasePrice',function($value){ return (string) $value; },[]],
					['count','./stk:stockHeader/stk:count',function($value){ return (string) $value; },[]],
					['ean','./stk:stockHeader/stk:EAN',function($value){ return (string) $value; },[]],
					['code','./stk:stockHeader/stk:code',function($value){ return (string) $value; },[]],
					['note','./stk:stockHeader/stk:nameComplement',function($value){ return (string) $value; },[]]
				],

			];

	}

}
