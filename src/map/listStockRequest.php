<?php

namespace Andering\MClient\map;

use DateTime;

class listStockRequest
{

	public $map = null;

	public function __construct()
	{

		$this->map =
			['stock','//lStk:stock',function($value){  return (object) $value; },
				[
					['stockHeader','./stk:stockHeader',function($value){ return (object) $value;},
						[
							['weightedPurchasePrice','./stk:weightedPurchasePrice',function($value){ return (string) $value; },[]],
							['count','./stk:count',function($value){ return (string) $value; },[]],
							['ean','./stk:EAN',function($value){ return (string) $value; },[]],
							['code','./stk:code',function($value){ return (string) $value; },[]]
						],
					]
				]
			];

	}

}
