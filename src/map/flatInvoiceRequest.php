<?php

namespace Andering\MClient\map;

use DateTime;

class flatInvoiceRequest
{

	public $map = null;

	public function __construct()
	{

		$this->map =
			['invoice','//lst:invoice',function($value){  return (object) $value; },
				[

					['id','./inv:invoiceHeader/inv:id',function($value){ return (string) $value; },[]],
					['date','./inv:invoiceHeader/inv:date',function($value){ return new DateTime($value); },[]],
					['item','./inv:invoiceDetail/inv:invoiceItem',function($value){ return (object) $value; },
						[
							['contract','./inv:contract/typ:ids',function($value){ return (string) $value; },[]],
							['quantity','./inv:quantity',function($value){ return (string) $value; },[]],
							['code','./inv:code',function($value){ return (string) $value; },[]],
							['note','./inv:note',function($value){ return (string) $value; },[]],
							['discountPercentage','./inv:discountPercentage',function($value){ return (string) $value; },[]],
							['ean','./inv:stockItem/typ:stockItem/typ:EAN',function($value){ return (string) $value; },[]],
							['price','./inv:homeCurrency/typ:price',function($value){ return (string) $value; },[]]
						]
					]
				]
			];
	}
}
