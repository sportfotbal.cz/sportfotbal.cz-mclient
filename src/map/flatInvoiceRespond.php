<?php

namespace Andering\MClient\map;

use DateTime;

class flatInvoiceRespond
{

	public $map = null;

	public function __construct()
	{

		$this->map =
			['invoice','//inv:invoiceResponse',function($value){  return (object) $value; },
				[
					['state','./@state',function($value){ return (string) $value; },[]],
					['id','./rdc:producedDetails/rdc:id',function($value){ return (string) $value; },[]],
					['number','./rdc:producedDetails/rdc:number',function($value){ return (string) $value; },[]],
					['importDetails','./rdc:importDetails/rdc:detail',function($value){ return (object) $value; },
						[
							['state','./rdc:state',function($value){ return (string) $value; },[]],
							['note','./rdc:note',function($value){ return (string) $value; },[]],
							['valueProduced','./rdc:valueProduced',function($value){ return (string) $value; },[]],
							['valueRequested','./rdc:valueRequested',function($value){ return (string) $value; },[]],
						]
					]
				]
			];
	}

}
