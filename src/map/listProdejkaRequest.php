<?php

namespace Andering\MClient\map;

use DateTime;

class listProdejkaRequest
{

	public $map = null;

	public function __construct()
	{

		$this->map =
			['prodejka','//lst:prodejka',function($value){  return (object) $value; },
				[
					['prodejkaHeader','./pro:prodejkaHeader',function($value){ return (object) $value;},
						[
							['id','./pro:id',function($value){ return (string) $value; },[]],
							['date','./pro:date',function($value){ return new DateTime($value); },[]],
							['contract','./pro:contract',function($value){ return (object) $value; },
								[
									['ids','./typ:ids',function($value){ return (string) $value; },[]]
								]
							],
						],
					],
					['prodejkaDetail','./pro:prodejkaDetail',function($value){  return (object) $value; },
						[
							['prodejkaItem','./pro:prodejkaItem',function($value){ return (object) $value; },
								[
									['quantity','./pro:quantity',function($value){ return (string) $value; },[]],
									['discountPercentage','./pro:discountPercentage',function($value){ return (string) $value; },[]],
									['stockItem','./pro:stockItem',function($value){ return (object) $value; },
										[
											['stockItem','./typ:stockItem',function($value){ return (object) $value; },
												[
													['ean','./typ:EAN',function($value){ return (string) $value; },[]],
												]
											]
										]
									],
									['homeCurrency','./pro:homeCurrency',function($value){ return (object) $value; },
										[
											['price','./typ:price',function($value){ return (string) $value; },[]]

										]
									]
								]
							],
						]
					]
				]
			];

	}
}
