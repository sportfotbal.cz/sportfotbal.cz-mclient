<?php

namespace Andering\MClient\map;

use DateTime;

class flatProdejkaRequest
{

	public $map = null;

	public function __construct()
	{

		$this->map =
			['prodejka','//lst:prodejka',function($value){  return (object) $value; },
				[

					['id','./pro:prodejkaHeader/pro:id',function($value){ return (string) $value; },[]],
					['date','./pro:prodejkaHeader/pro:date',function($value){ return new DateTime($value); },[]],
					['item','./pro:prodejkaDetail/pro:prodejkaItem',function($value){ return (object) $value; },
						[
							['contract','./../../pro:prodejkaHeader/pro:contract/typ:ids',function($value){ return (string) $value; },[]],
							['quantity','./pro:quantity',function($value){ return (string) $value; },[]],
							['code','./pro:code',function($value){ return (string) $value; },[]],
							['note','./pro:note',function($value){ return (string) $value; },[]],
							['discountPercentage','./pro:discountPercentage',function($value){ return (string) $value; },[]],
							['ean','./pro:stockItem/typ:stockItem/typ:EAN',function($value){ return (string) $value; },[]],
							['price','./pro:homeCurrency/typ:price',function($value){ return (string) $value; },[]]
						]
					]
				]
			];

	}
}


